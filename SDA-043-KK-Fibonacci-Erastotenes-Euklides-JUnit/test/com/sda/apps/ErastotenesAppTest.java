package com.sda.apps;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

public class ErastotenesAppTest {
	@Test
	public void testErastotenes() {

		// test metody na liczbach
		List<Integer> arrayOfPrimesWithLimit = ErastotenesApp.findPrimesOnIntegers(100);

		// test metody na booleanach
		// List<Integer> arrayOfPrimesWithLimit =
		// ErastotenesApp.findPrimesOnBooleans(100);

		// sprawdzamy czy nasza przykladowa mala lista roznych liczb pierwszych
		// zawiera sie w zbiorze
		// mozna tez pisac primesToCheck.add(13) itd. to ponizej to jest
		// sprytniejszy sposob na wpisywanie licz w ArrayList
		List<Integer> primesToCheck = Arrays.asList(13, 29, 53, 97);

		// sposob 1 - zapis Kaspera z wykrzyknikiem
		// for (int i = 0; i < primesToCheck.size(); i++) {
		// if (!result.contains(primesToCheck.get(i))) {
		// fail();
		// }
		// }
		
		// sposob 1 - zapis moj przez zwykly false
		for (int i = 0; i < primesToCheck.size(); i++) {

			if (arrayOfPrimesWithLimit.contains(primesToCheck.get(i)) == false) {

				fail();
			}

		// sposob 2 - przez assert
		// assertTrue(arrayOfPrimesWithLimit.contains(13) == true);
		// assertTrue(arrayOfPrimesWithLimit.contains(29) == true);
		// assertTrue(arrayOfPrimesWithLimit.contains(53) == true);
		// assertTrue(arrayOfPrimesWithLimit.contains(97) == true);

		}
	}
}
