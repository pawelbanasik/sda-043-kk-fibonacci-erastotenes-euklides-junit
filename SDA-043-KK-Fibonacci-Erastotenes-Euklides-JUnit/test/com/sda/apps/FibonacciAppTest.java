package com.sda.apps;

import static org.junit.Assert.*;

import org.junit.Test;

public class FibonacciAppTest {

	@Test
	public void testFibonacciOnBigArray(){
		assertTrue(FibonacciApp.fibonacciSeriesOnBigArray(20) == 6765);
	}
}
