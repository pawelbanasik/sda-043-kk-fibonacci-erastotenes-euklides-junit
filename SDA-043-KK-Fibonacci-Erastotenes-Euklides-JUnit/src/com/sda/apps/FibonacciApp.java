package com.sda.apps;

public class FibonacciApp {

	public static void main(String[] args) {

		fibonacciSeriesOnBigArray(20);
		fibonacciSeriesOnSmallArray(20);
	}

	 public static int fibonacciSeriesOnBigArray(int index) {
	
	 // jak wiekszy index niz 47 to overflow
	 // mniej niz zero nie moze byc
	 if(index >= 47 || index <=0){
	 System.out.println("Incorrect Index");
	 return 0;
	 }
	
	 int[] array = new int[index];
	 array[0] = 1;
	 array[1] = 1;
	
	 for (int i = 2; i < 20; i++) {
	
	 array[i] = array[i - 2] + array[i - 1];
	
	 }
	
	 System.out.println("The " + index + "th number of Fibonacci series is " +
	 array[index - 1] + ". Using big array.");
	 return array[index - 1];
	
	 }
	public static int fibonacciSeriesOnSmallArray(int index) {
		
		if(index >= 47 || index <=0){
			System.out.println("Incorrect Index");
			return 0;
		}
		
		int[] array = new int[3];
		array[0] = 1;
		array[1] = 1;

		for (int i = 2; i < index; i++) {

			array[2] = array[0] + array[1];
			array[0] = array[1];
			array[1] = array[2];

		}

		System.out.println("The " + index + "th number of Fibonacci series is " + array[2] + ". Using small array.");
		return array[2];

	}
}
