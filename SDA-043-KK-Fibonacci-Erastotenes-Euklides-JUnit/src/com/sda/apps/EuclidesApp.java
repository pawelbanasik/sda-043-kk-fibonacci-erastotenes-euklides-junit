package com.sda.apps;

import java.util.Arrays;

public class EuclidesApp {

	public static void main(String[] args) throws Exception {
		int[] array = new int[]{28,14,56,2,14};
		
		findGreatestCommonDivider(5,12);
		findGreatestCommonDividerForGroup(array);
	}
	
	static int findGreatestCommonDividerForGroup(int[] array) throws Exception{
		
		if(array.length != 5){
			throw new Exception("Invalid length of array. Expected: 5");
		}
		
		Arrays.sort(array);
		for(int i = 0; i < array.length; i++){
			System.out.println("Array element: "+array[i]);
		}
			/*lowest*/	  /*greatest*/
		while(array[0] != array[array.length-1]){
			array[array.length-1] = array[array.length-1] - array[0];
			Arrays.sort(array);
		}
		
		System.out.println(array[0]);
		return array[0];
	}
	
	static int findGreatestCommonDivider(int a, int b){

		while(a != b){
			if(a > b){
				a = a - b;
			} else {
				b = b - a;
			}
		}
		return a;
	}

}
