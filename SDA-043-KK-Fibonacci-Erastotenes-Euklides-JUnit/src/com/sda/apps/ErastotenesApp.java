package com.sda.apps;

import java.util.ArrayList;
import java.util.List;

public class ErastotenesApp {

	/**
	 * Hide constructor
	 */
	private ErastotenesApp(){
		
	}
	
	public static void main(String[] args) {
		
		findPrimesOnIntegers(100);
		
	}
	
	public static List<Integer> findPrimesOnBooleans(int limit){
		List<Integer> primeNumbers = new ArrayList<Integer>();
		
		boolean[] array = new boolean[limit];
		
		array[0] = true;
		array[1] = true;
		
		for(int i = 2; i < limit; i++){
			array[i] = false;
		}
		
		for(int j = 2; j < limit; j++){
			for(int k = 2 ; k * j < limit; k++){
				array[k*j] = true;
			}
		}
		
		for(int i = 0; i < array.length; i++){
			if(array[i] != true)
				primeNumbers.add(i);
		}
		
		return primeNumbers;
		
	}

	public static List<Integer> findPrimesOnIntegers(int limit){
		List<Integer> primeNumbers = new ArrayList<Integer>();
		
		int[] array = new int[limit];
		for(int i = 0; i < limit; i++){
			array[i] = i;
		}
		
		for(int j = 2; j < limit; j++){
			for(int k = 2 ; k * j < limit; k++){
				array[k*j] = -1;
			}
		}
		
		for(int i = 2; i < array.length; i++){
			if(array[i] != -1)
				primeNumbers.add(i);
		}
		

		
		for (int element : primeNumbers) {
			System.out.println(element);
		}
		return primeNumbers;
		
	}

}
